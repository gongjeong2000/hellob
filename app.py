from flask import Flask
from flask import request
app = Flask(__name__)

@app.route('/')
def root():
    return 'hello Bob from GongJB'
@app.route('/add')
def add():
    a=request.args.get('a')
    b=request.args.get('b')
    return str(int(a)+int(b))
@app.route('/sub')
def sub():
    a=request.args.get('a')
    b=request.args.get('b')
    return str(int(a)-int(b))
if __name__ == '__main__':  
   app.run('0.0.0.0', port=8010)