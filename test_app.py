import unittest
import requests
import random

class Test1(unittest.TestCase):    
    def test_add(self):
        a=random.randint(-100, 100)
        b=random.randint(-100, 100)
        response=requests.get(f'http://13.209.15.210:8010/add?a={a}&b={b}')
        st=response.text
        self.assertEqual(a+b, int(st))
        
    def test_sub(self):
        a=random.randint(-100, 100)
        b=random.randint(-100, 100)
        response=requests.get(f'http://13.209.15.210:8010/sub?a={a}&b={b}')
        st=response.text
        self.assertEqual(a-b, int(st))

if __name__=="__main__":
    unittest.main()